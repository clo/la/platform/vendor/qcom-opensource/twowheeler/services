/* Copyright (c) 2022 Qualcomm Innovation Center, Inc. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted (subject to the limitations in the
 * disclaimer below) provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the following
 *    disclaimer in the documentation and/or other materials provided
 *    with the distribution.
 *
 *  * Neither the name of Qualcomm Innovation Center, Inc. nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE
 * GRANTED BY THIS LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT
 * HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package android.car.hardware;

import android.car.Car;
import android.car.CarManagerBase;
import android.car.CarNotConnectedException;
import android.car.hardware.CarPropertyConfig;
import android.car.hardware.CarPropertyValue;
import android.car.hardware.property.CarPropertyManager;
import android.car.hardware.property.CarPropertyManager.CarPropertyEventCallback;
import android.car.hardware.property.ICarProperty;
import android.content.Context;
import android.os.Handler;
import android.os.IBinder;
import android.util.ArraySet;
import android.util.Log;

import android.annotation.IntDef;
import android.annotation.NonNull;
import android.annotation.Nullable;
import android.annotation.SystemApi;

import com.android.internal.annotations.GuardedBy;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class TwoWheelerManager extends CarManagerBase {
    private final static boolean DBG = false;
    private final String TAG = "TwoWheelerManager";
    private final CarPropertyManager mTwoWheelerPropertyMgr;

    @GuardedBy("mLock")
    private final ArraySet<TwoWheelerEventCallback> mCallbacks = new ArraySet<>();
    private final Object mLock = new Object();

    @GuardedBy("mLock")
    private TwoWheelerPropertyEventListenerToBase mListenerToBase = null;

    public interface TwoWheelerEventCallback {
        /**
         * Called when a property is updated
         * @param value Property that has been updated.
         */
        void onChangeEvent(@NonNull CarPropertyValue value);

        /**
         * Called when an error is detected with a property
         * @param propertyId
         * @param zone
         */
        void onErrorEvent(int propertyId, int zone);
    }

    private static class TwoWheelerPropertyEventListenerToBase implements CarPropertyEventCallback {
        private final WeakReference<TwoWheelerManager> mManager;

        TwoWheelerPropertyEventListenerToBase(TwoWheelerManager manager) {
              mManager = new WeakReference<>(manager);
        }

        @Override
        public void onChangeEvent(CarPropertyValue value) {
            TwoWheelerManager manager = mManager.get();
            if (manager != null) {
                manager.handleOnChangeEvent(value);
            }
        }

        @Override
        public void onErrorEvent(int propertyId, int zone) {
            TwoWheelerManager manager = mManager.get();
            if (manager != null) {
                manager.handleOnErrorEvent(propertyId, zone);
            }
        }
    }

    private void handleOnChangeEvent(CarPropertyValue value) {
        Collection<TwoWheelerEventCallback> callbacks;
        synchronized (mLock) {
            callbacks = new ArrayList<>(mCallbacks);
        }
        for (TwoWheelerEventCallback l: callbacks) {
            l.onChangeEvent(value);
        }
    }

    private void handleOnErrorEvent(int propertyId, int zone) {
        Collection<TwoWheelerEventCallback> listeners;
        synchronized (mLock) {
            listeners = new ArrayList<>(mCallbacks);
        }
        for (TwoWheelerEventCallback l: listeners) {
            l.onErrorEvent(propertyId, zone);
        }
    }

     /**
      * Creates an instance of the {@link CarVendorExtensionManager}.
      *
      * <p>Should not be obtained directly by clients, use {@link Car#getCarManager(String)} instead.
      * @hide
      */
    public TwoWheelerManager(@NonNull Car car, @NonNull IBinder service){
        super(car);
        ICarProperty mCarPropertyService = ICarProperty.Stub.asInterface(service);
        mTwoWheelerPropertyMgr = new CarPropertyManager(car, mCarPropertyService);
    }

    /** Registers listener. The methods of the listener will be called when new events arrived in
     * the main thread.
     */
    public void registerCallback(@NonNull TwoWheelerEventCallback callback) {
        synchronized (mLock) {
            if (mCallbacks.isEmpty()) {
                mListenerToBase = new TwoWheelerPropertyEventListenerToBase(this);
            }
            List<CarPropertyConfig> configs = mTwoWheelerPropertyMgr.getPropertyList();
            for (CarPropertyConfig c : configs) {
                // Register each individual propertyId
                mTwoWheelerPropertyMgr.registerCallback(mListenerToBase, c.getPropertyId(), 0);
            }
            mCallbacks.add(callback);
        }
    }


    /** Unregisters listener that was previously registered. */
    public void unregisterCallback(@NonNull TwoWheelerEventCallback callback) {
        synchronized (mLock) {
            mCallbacks.remove(callback);
            List<CarPropertyConfig> configs = mTwoWheelerPropertyMgr.getPropertyList();
            for (CarPropertyConfig c : configs) {
                // Register each individual propertyId
                mTwoWheelerPropertyMgr.unregisterCallback(mListenerToBase, c.getPropertyId());
            }
            if (mCallbacks.isEmpty()) {
                mListenerToBase = null;
            }
          }
      }

    @NonNull
    public List<CarPropertyConfig> getPropertyList() throws CarNotConnectedException {
        return mTwoWheelerPropertyMgr.getPropertyList();
    }

    public boolean isPropertyAvailable(int propertyId, int area)
            throws CarNotConnectedException {
        return mTwoWheelerPropertyMgr.isPropertyAvailable(propertyId, area);
    }

    public boolean getBooleanProperty(int propertyId, int area)
            throws CarNotConnectedException {
        return mTwoWheelerPropertyMgr.getBooleanProperty(propertyId, area);
    }

    public float getFloatProperty(int propertyId, int area)
            throws CarNotConnectedException {
        return mTwoWheelerPropertyMgr.getFloatProperty(propertyId, area);
    }

    public int getIntProperty(int propertyId, int area)
            throws CarNotConnectedException {
        return mTwoWheelerPropertyMgr.getIntProperty(propertyId, area);
    }

    public void setBooleanProperty(int propertyId, int area, boolean val)
            throws CarNotConnectedException {
        mTwoWheelerPropertyMgr.setBooleanProperty(propertyId, area, val);
    }

    public void setFloatProperty(int propertyId, int area, float val)
            throws CarNotConnectedException {
        mTwoWheelerPropertyMgr.setFloatProperty(propertyId, area, val);
    }

    public void setIntProperty(int propertyId, int area, int val)
            throws CarNotConnectedException {
        mTwoWheelerPropertyMgr.setIntProperty(propertyId, area, val);
    }

    /** @hide */
    @Override
    public void onCarDisconnected() {
        synchronized (mLock) {
            mCallbacks.clear();
        }
        mTwoWheelerPropertyMgr.onCarDisconnected();
    }
}
